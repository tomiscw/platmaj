﻿using System;

namespace PlatMaj
{
    public class Guessing
    {
        public static void Game()
        {
            var guess = 0;
            var rand = new Random().Next(25) + 1;

            Console.WriteLine("== Guessing Game ==");
            // Console.WriteLine($"[Debug] Secret: {rand}");

            while (guess != rand)
            {
                var input = int.TryParse(Console.ReadLine(), out guess);
                if (guess < rand) Console.WriteLine("Too low.");
                else if (guess > rand) Console.WriteLine("Too high.");
                else if (guess == rand)
                {
                    Console.WriteLine($"Correct! The answer was {rand}.");
                    Console.ReadKey();
                    Environment.Exit(Environment.ExitCode);
                }
                else Console.WriteLine("I didn't understand that.");
            }
        }
    }
}
