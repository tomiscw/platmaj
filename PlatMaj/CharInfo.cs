﻿using System;

namespace PlatMaj
{
    public class CharInfo
    {
        string Name { get; set; }
        string Sex { get; set; }
        string Species { get; set; }
        bool Fanart { get; set; }


        public CharInfo(string name, string sex,
            string species, bool fanart)
        {
            Name = name;
            Sex = sex;
            Species = species;
            Fanart = fanart;
        }

        public CharInfo() { }

        public void WriteInfo()
        {
            Console.WriteLine($"Name: {Name}{Environment.NewLine}Sex: {Sex}");
            Console.WriteLine($"Species: {Species}{Environment.NewLine}Fanart: {Fanart}");
            Console.Write($"{Environment.NewLine}");
        }

        public void Interactive()
        {
            string is_sex;
            string is_fanart;
            Console.Clear();
            Console.WriteLine($"== Interactive mode ==");
            Console.Write($"Character name: ");
            Name = Console.ReadLine();
            Console.Write($"Character sex: ");
            is_sex = Console.ReadLine();
            switch (is_sex)
            {
                case "M":
                case "m": Sex = "Male"; break;
                case "F":
                case "f": Sex = "Female"; break;
                default: Sex = is_sex; break;
            };
            Console.Write($"Character species: ");
            Species = Console.ReadLine();
            Console.Write($"Character fanart (y/n): ");
            is_fanart = Console.ReadLine();
            switch (is_fanart)
            {
                case "y": Fanart = true; break;
                default:
                case "n": Fanart = false; break;
            };
            Console.Write($"{Environment.NewLine}");
            WriteInfo();
            Console.ReadKey();
            Environment.Exit(Environment.ExitCode);
        }
    }
}
