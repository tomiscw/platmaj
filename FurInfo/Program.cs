﻿using System;
using PlatMaj;

namespace FurInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            string is_interactive;
            var tomis = new CharInfo("Tomis CW", "Male", "Fennec", true);
            var tango = new CharInfo("Tango Ringtail", "Male", "Zecoon", true);
            var winter = new CharInfo("Winter Green", "Male", "German Shepherd", false);

            tomis.WriteInfo();
            tango.WriteInfo();
            winter.WriteInfo();

            Console.WriteLine("Interactive mode? Y/N");
            is_interactive = Console.ReadLine();

            switch (is_interactive)
            {
                case "y":
                    var idemo = new CharInfo();
                    idemo.Interactive();
                    break;
                default:
                case "n":
                    Environment.Exit(Environment.ExitCode);
                    break;
            };
        }
    }
}
